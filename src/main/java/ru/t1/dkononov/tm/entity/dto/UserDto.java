package ru.t1.dkononov.tm.entity.dto;

import ru.t1.dkononov.tm.entity.model.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class UserDto {

    private String id = UUID.randomUUID().toString();

    private String login;

    private String passwordHash;

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    private List<Role> roles = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

}
