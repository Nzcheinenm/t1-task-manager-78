package ru.t1.dkononov.tm.api.service;

import ru.t1.dkononov.tm.entity.dto.TaskDto;

import java.util.List;

public interface TaskDtoService {
    List<TaskDto> findAll(String userId);

    TaskDto save(TaskDto task);

    TaskDto save(String userId);

    TaskDto findById(String id);

    boolean exsistsById(String id);

    TaskDto findByUserIdAndId(String userId, String id);

    boolean exsistsByUserIdAndId(String userId, String id);

    void deleteByUserIdAndId(String userId, String id);

    long count();

    void deleteById(String id);

    void delete(TaskDto task);

    void deleteAll(List<TaskDto> tasks);

    void clear();
}
