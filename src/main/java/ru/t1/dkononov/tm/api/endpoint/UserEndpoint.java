package ru.t1.dkononov.tm.api.endpoint;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.dkononov.tm.entity.dto.UserDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface UserEndpoint {
    @WebMethod
    @GetMapping("/findAll")
    List<UserDto> findAll();

    @WebMethod
    @PostMapping("/save")
    UserDto save(
            @WebParam(name = "user", partName = "user")
            @RequestBody UserDto user
    );

    @WebMethod
    @GetMapping("/findById/{id}")
    UserDto findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/exsitsById/{id}")
    boolean exsistsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "user", partName = "user")
            @RequestBody UserDto user
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "users", partName = "users")
            @RequestBody List<UserDto> users
    );

    @WebMethod
    @PostMapping("/clear")
    void clear();
}
