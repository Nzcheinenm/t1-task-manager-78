package ru.t1.dkononov.tm.api.service;

import ru.t1.dkononov.tm.entity.dto.UserDto;

import java.util.List;

public interface UserService {
    List<UserDto> findAll();

    UserDto save(UserDto user);

    UserDto findById(String id);

    boolean exsistsById(String id);

    UserDto save();

    long count();

    void deleteById(String id);

    void delete(UserDto project);

    void deleteAll(List<UserDto> projectsDto);

    void clear();
}
