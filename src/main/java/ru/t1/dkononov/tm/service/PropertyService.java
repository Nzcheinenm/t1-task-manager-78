package ru.t1.dkononov.tm.service;

import lombok.Getter;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.dkononov.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

//    @Value("#{environment['password.iteration']}")
//    public Integer passwordIteration;
//
//    @Value("#{environment['server.port']}")
//    public Integer serverPort;
//
//    @Value("#{environment['server.host']}")
//    public String serverHost;
//
//
//    @Value("#{environment['password.secret']}")
//    public String passwordSecret;
//
//
//    public final Properties properties = new Properties();

    public PropertyService() {
    }

}
