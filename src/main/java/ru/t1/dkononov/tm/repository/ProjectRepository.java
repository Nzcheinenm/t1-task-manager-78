package ru.t1.dkononov.tm.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkononov.tm.entity.model.Project;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository extends JpaRepository<Project, String> {

    Optional<Project> findByUserIdAndId(String userId, String id);

    @Modifying
    @Transactional
    void deleteByUserIdAndId(String userId, String id);

    List<Project> findAllByUserId(String userId);

}
