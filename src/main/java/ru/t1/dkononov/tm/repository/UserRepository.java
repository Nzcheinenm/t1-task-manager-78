package ru.t1.dkononov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.t1.dkononov.tm.entity.model.User;

public interface UserRepository extends JpaRepository<User, String> {

    User findByLogin(String login);

}
