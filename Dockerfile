FROM openjdk:8
MAINTAINER Dmitry Kononov
WORKDIR /opt
EXPOSE 8080
EXPOSE 61616
COPY target/ ./
ENTRYPOINT ["java", "-jar", "task-manager-1.0.0.jar"]
